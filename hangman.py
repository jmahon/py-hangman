
import os
import sys

# Used to reset the hangman
hangmanblank =  [[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "]]

# Used as the 'pixel display' for the hangman
hangman =  [[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "],
			[" "," "," "," "," "," "]]

# Used as a template of the hangman
hangmanparts =  [[" "," ","_","_"," "," "], #0
			[" ","|"," "," ","|"," "], #1
			[" ","|"," "," ","O"," "], #2
			[" ","|"," ","-","|","-"], #3
			[" ","|"," ","/"," ","\\"], #4
			["_","|","_","_","_"," "], #5
			[" "," "," "," "," "," "]] #6

# Lists of tuple references. Each refers to a section of the hangman drawing.
wrongsets =[[(5,0),(5,2),(5,3),(5,4)],			# _ ground
			[(1,1),(2,1),(3,1),(4,1),(5,1)],	# | pole
			[(0,1),(0,2)],						# _	top bar
			[(1,4)],							# | rope
			[(2,4)],							# O head
			[(3,4)],							# | torsop
			[(4,3)],							# / left leg
			[(4,5)],							# \ right leg
			[(3,3)],							# - left arm
			[(3,5)]]							# - right arm


stayplaying = True


def wrongletter(wrongcounter):
	'''Adds 1 to the wrong letter count. Wrong letter count is used
	to work out which the next part of the hangman to draw
	'''
	wrongcounter +=1
	return wrongcounter


def replaceunderscores(halfcompleted, letter, word):
	'''Replaces the underscores in the word when a correct letter has been
	guessed.
	'''
	counter = 0
	for i in word:
		if i == letter:
			halfcompleted[counter] = letter
		counter +=1
	return halfcompleted


def gamewin(word):
	'''Runs when the game has been won. Clears the screen and triggers a prompt
	for the user to play a new game
	'''
	os.system('clear')
	print("")
	print("")
	print("")
	print("          YOU WIN!")
	print("")
	print("     The word was "+ word +"!")
	newgamequestion()
	return


def gameover(word):
	'''Runs when the game has been lost. Clears the screen and triggers a prompt
	for the user to play a new game
	'''
	print("")
	print("     YOU LOSE!")
	print("")
	print("     The word was "+ word +"!")
	newgamequestion()
	return


def newgamequestion():
	'''Asks the user whether they want to play a new game'''
	print("")
	startnewgame = input("     Play a new game? (y/n): ")
	if startnewgame == "n" or startnewgame == "N":
		os.system('clear')
		exit()
	else:
		os.system('clear')
		return


def provideword():
	'''Runs at the start to obtain the word to play with'''
	os.system('clear')
	print("")
	print("")
	print("     Welcome to Hangman!")
	print("")
	word = input("     Please enter a word: ")
	os.system('clear')
	return word


def main():
	'''Main thread. Spaghetti with bad variable names'''

	# Reset all the guesses, wrong answers etc from the previous game
	os.system('clear')
	guesses = []
	wrongcounter = 0
	halfcompleted = []

	# Reset the blank hangman template
	row = 0
	col = 0
	for x in hangmanblank:
		for y in x:
			hangmanblank[row][col] = " "
			col+=1
		row+=1
		col = 0
	hangman = list(hangmanblank)

	refreshandshow(wrongcounter, halfcompleted, guesses)
	word = provideword()
	for i in range(0, len(word)):
		halfcompleted.append("_")
	play = True
	while play == True:
		refreshandshow(wrongcounter, halfcompleted, guesses)
		acceptableguess = False
		if "".join(halfcompleted) == word:
			gamewin(word)
			return
		while acceptableguess == False:
			print("")
			letterguess = input("    Guess a letter: ")
			if letterguess == word:
				gamewin(word)
				return
			elif len(letterguess) == 1:
				acceptableguess = True
			else:
				refreshandshow(wrongcounter, halfcompleted, guesses)
		if "".join(halfcompleted) == word:
			gamewin(word)
			return
		if letterguess not in word:
			if letterguess not in guesses:
				wrongcounter = wrongletter(wrongcounter)
				addhangmanparts(wrongcounter)
		else:
			pass
			replaceunderscores(halfcompleted, letterguess, word)
		guesses.append(letterguess)
		refreshandshow(wrongcounter, halfcompleted, guesses)
		if wrongcounter == 10:
			gameover(word)
			return
	return


def addhangmanparts(wrongcounter):
	'''Work out which parts to add to the hangman image based on the number of
	wrong answers so far
	'''
	for i in wrongsets[wrongcounter-1]:
		row = i[0]
		col = i[1]
		hangman[row][col] = str(hangmanparts[row][col])
		if wrongcounter-1 == 0:
			hangman[5][1] = "_"


def refreshandshow(wrongcounter, halfcompleted, guesses):
	'''Refresh the screen and write the hangman image and wrong guesses etc'''
	os.system('clear')
	print("")
	print("")
	# Commented out sys colour codes for win/mac compatability.
	#sys.stdout.write("\033[0;32m")
	for row in hangman:
		print("        "+"".join(row))
	# Commented out sys colour codes for win/mac compatability.
	#sys.stdout.write("\033[0;0m")
	print("        "+"".join(halfcompleted))
	print("")
	print("")
	print("     Wrong letters   : ", wrongcounter)
	print("     Previous guesses: ", ", ".join(guesses))


# Runs the main game loop
while stayplaying == True:
	hangman = list(hangmanblank)
	main()

